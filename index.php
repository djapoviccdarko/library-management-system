<?php

// Include Composer's autoloader
use App\AppContainer;

require_once __DIR__ . '/vendor/autoload.php';

// Instantiate the container
$appContainer = new AppContainer();
$container = $appContainer->getContainer();

// Retrieve the session_start() and SimpleRouter instances from the container
$sessionStart = $container->get('session_start');
$router = $container->get('simple_router');

// Start the router
$router->start();
