<?php

use Pecee\SimpleRouter\SimpleRouter;

// Home Controller routes
SimpleRouter::get('/homePage', 'HomeController@homePage');

// Book Controller routes
SimpleRouter::get('/showAllBooks', 'BookController@showAll');
SimpleRouter::get('/insertPage', 'BookController@insertPage');
SimpleRouter::post('/insertBook', 'BookController@insert');
SimpleRouter::get('/updatePage', 'BookController@updatePage');
SimpleRouter::post('/updateBook', 'BookController@update');
SimpleRouter::get('/deleteBook', 'BookController@delete');

// User Controller routes
SimpleRouter::get('/getAllUsers', 'UserController@getAll');
SimpleRouter::get('/updateUser', 'UserController@update');
SimpleRouter::get('/deleteUser', 'UserController@delete');

// Author Controller routes
SimpleRouter::post('/insertAuthor', 'AuthorController@insert');

// Book Category Controller routes
SimpleRouter::post('/insertCategory', 'CategoryController@insert');

// User Auth Controller routes
SimpleRouter::get('/loginPage', 'UserAuthController@loginPage');
SimpleRouter::get('/registerPage', 'UserAuthController@registerPage');
SimpleRouter::post('/insertUser', 'UserAuthController@insert');
SimpleRouter::post('/verifyEmail', 'UserAuthController@verifyEmail');
SimpleRouter::get('/verifyPage', 'UserAuthController@verifyPage');
SimpleRouter::post('/userLogin', 'UserAuthController@login');
SimpleRouter::get('/userLogout', 'UserAuthController@logout');
