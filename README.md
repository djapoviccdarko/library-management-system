# Library Management System

## Name
Library Management System

## Description
This management system allows registered users to purchase books. To register, users must create an account and confirm their email address before logging in.
A confirmation message will be sent to the email provided during registration.
The administrator has the authority to approve or reject account requests and can also add, edit, or delete books in the system.
Each book in the system will have a name, author, category, description, price, and downloadable PDF format available for users who have completed payment.
Users can add multiple books to their shopping cart and purchase them together.
The project will include the following features:
User Reviews - Users can leave reviews and ratings for books they have purchased, helping others make informed decisions about what to buy.
Wishlist - Users can create a wishlist of books they are interested in but not yet ready to purchase, making it easier for them to keep track of books they want to buy in the future.
Social Sharing - Users can share their purchases and reviews on social media platform such as Facebook, helping to promote the system to a wider audience.

## Usage
Online bookshop were you can buy a book in PDF format.

## Authors and acknowledgment
Darko Djapovic

## Project status
In progress.
