<?php

namespace App;

use DI\ContainerBuilder;

class AppContainer
{
    private mixed $container;

    public function __construct()
    {
        // Instantiate a new container builder
        $containerBuilder = new ContainerBuilder();

        // Build the container
        $this->container = $containerBuilder->build();

        // Register the session_start() service
        $this->container->set('session_start', function () {
            return session_start();
        });

        // Register the SimpleRouter service
        $this->container->set('simple_router', function () {
            $router = new \Pecee\SimpleRouter\SimpleRouter();
            $router->setDefaultNamespace('App\src\Controllers');
            require_once './routes.php';
            return $router;
        });
    }

    public function getContainer()
    {
        return $this->container;
    }
}
