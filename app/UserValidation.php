<?php

namespace App;

use App\src\Controllers\BaseController;
use App\src\Controllers\UserController;
use Exception;
use Valitron\Validator;

class UserValidation extends BaseController
{
    /**
     * Validates user input data and inserts it into the database if it passes validation.
     * Renders the user login page if validation is successful, or the user registration page with errors and old inputs if validation fails.
     * @throws /Exception
     */
    public function validate(): void
    {
        // Create a new Validator instance and initialize it with the user input ($_POST data).
        $validator = new Validator($_POST);

        // Create a new UserController instance to access user data.
        $userController = new UserController();

        // Define the validation rules for the user input.
        $validator->rule('required', [
            'name',
            'lastname',
            'address',
            'email',
            'username',
            'password',
            'repeat_password',
            'phone',
            'city'
        ]);
        // Check if name contains only alphabetical characters and spaces.
        $validator->rule('regex', 'name', '/^[a-zA-Z\s]+$/');

        // Check if lastname contains only alphabetical characters and spaces.
        $validator->rule('regex', 'lastname', '/^[a-zA-Z\s]+$/');

        // Check if address contains only alphabetical characters, spaces, and digits.
        $validator->rule('regex', 'address', '/^[a-zA-Z\s\d]+$/');

        // Check if email is a valid email address.
        $validator->rule('email', 'email');

        // Validate if the email is unique.
        $validator->rule(
            function ($field, $value) use ($userController) {
                return !$userController->selectByEmail($value);
            },
            'email'
        )->message('Email is already in use!');

        // Validate if the username is unique.
        $validator->rule(
            function ($field, $value) use ($userController) {
                return !$userController->selectByUsername($value);
            },
            'username'
        )->message('Username is already in use!');

        // Add a rule to check that the username is at least 4 characters long
        $validator->rule('lengthMin', 'username', 4);

        // Add a rule to check that the username contains only alphanumeric characters and underscores
        $validator->rule('regex', 'username', '/^[a-zA-Z0-9_]+$/');

        // Check if password is at least 8 characters long.
        $validator->rule('lengthMin', 'password', 8)->message('Password must be at least 8 characters long.');

        // Check if password contains at least one uppercase letter.
        $validator->rule('regex', 'password', '/[A-Z]/u')->message('Password must contain at least one uppercase letter.');

        // Check if password contains at least one lowercase letter.
        $validator->rule('regex', 'password', '/[a-z]/u')->message('Password must contain at least one lowercase letter.');

        // Check if password contains at least one digit.
        $validator->rule('regex', 'password', '/\d/')->message('Password must contain at least one number.');

        // Check if password contains at least one special character.
        $validator->rule('regex', 'password', '/[^\w\s]/')->message('Password must contain at least one special character.');

        // Validate if the password matches the repeated password.
        $validator->rule('equals', 'repeat_password', 'password');

        // Check if phone contains only digits, spaces, and dashes.
        $validator->rule('regex', 'phone', '/^[\d\s-]+$/');

        // Check if city contains only alphabetical characters and spaces.
        $validator->rule('regex', 'city', '/^[a-zA-Z\s]+$/');

        // If validation is successful, insert the user details in the database.
        if ($validator->validate()) {
            $this->userModel->insert(
                $_POST['name'],
                $_POST['lastname'],
                $_POST['address'],
                $_POST['email'],
                $_POST['username'],
                password_hash($_POST['password'], PASSWORD_DEFAULT),
                $_POST['phone'],
                $_POST['city'],

            );

            //Mail Verification
            MailVerification::sendVerificationEmail($_POST['name'], $_POST['email']);

            // Render the template with the POST data and verificationTime
            $data = $_POST;
            $verificationTime = $this->userModel->getVerificationTime($_POST['email']);

            echo $this->twig->render('users/user-login.twig', ['data' => $data, 'verificationTime' => $verificationTime]);
            exit;
        }

        // If validation fails, collect the validation errors and render the user registration page with errors and old inputs.
        $oldInputs = $_POST;
        $errors = $validator->errors();

        echo $this->twig->render('users/user-register.twig', [
            'errors' => $errors,
            'oldInputs' => $oldInputs
        ]);
    }

    /**
     * Validates the user's login credentials and sets the session and cookie variables.
     *
     * @return void The method does not return a value.
     * @throws Exception
     */
    public function loginValidation(): void
    {
        // Retrieve the username and password from the POST request, or set them to empty strings if they are not set.
        $username = $_POST["username"] ?? "";
        $password = $_POST["password"] ?? "";

        // If either the username or password is empty, display an error message and exit.
        if ($username === "" || $password === "") {
            $msg = "Fill in all fields!";
            echo $this->twig->render('users/user-login.twig', [
                'msg' => $msg
            ]);
            return;
        }

        // Attempt to retrieve the user object by username and password.
        $user = $this->userModel->getByUsernameAndPassword($username, $password);

        // If the user object is found, set the session variables and optionally the user cookie.
        if ($user) {
            // If the "remember me" checkbox is checked, set the user cookie.
            if (isset($_POST['remember'])) {
                $userCookie = array('username' => $_POST['username'], 'password' => $_POST['password']);
                setcookie("userJSON", json_encode($userCookie), time() + 3600, "/");
            }

            // If the user has not verified their account, display a message and exit.
            if (!$this->userModel->checkVerificationByUsername($username)) {
                $msg = "Verify your account!";
                echo $this->twig->render('users/user-login.twig', [
                    'msg' => $msg
                ]);
                return;
            }
            // If a user with credentials exists, add it to the session and display it on the home page
            $_SESSION['user'] = $user;
            $_SESSION['last-active'] = time();
            echo $this->twig->render('home-page.twig', [
                'session' => $_SESSION
            ]);

            // If the user object is not found, display an error message and exit.
        } else {
            $msg = "Wrong login parameters!";
            echo $this->twig->render('users/user-login.twig', [
                'msg' => $msg
            ]);
        }
    }


}