<?php

namespace App;

use Doctrine\DBAL\Exception;

class Helpers
{
    /**
     * Render the view with the given data
     *
     * @param string $view The name of the view file to be rendered
     * @param array $data An array of data to be passed to the view
     * @throws Exception
     */
    public static function view(string $view, array $data)
    {
        // Extract the variables from the data array, so they can be used in the view
        extract($data);

        // Include the view file
        require_once "./app/src/Views/" . $view . ".php";
    }

    /**
     * Redirect to the specified URL
     */
    public static function redirect($url)
    {
        header("Location: $url");
        exit();
    }
}