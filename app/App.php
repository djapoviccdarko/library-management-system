<?php

namespace App;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class App
{

    /**
     * Returns a new instance of the Twig\Environment class for rendering Twig templates.
     *
     * @return Environment An instance of the Twig\Environment class.
     */
    public static function twigLoader(): Environment
    {
        $loader = new FilesystemLoader(Config::VIEW_PATH);
        return new \Twig\Environment($loader, [
            // enable the DebugExtension
            'debug' => true,
        ]);
    }
}