<?php

namespace App\src\Models;

use App\src\Database\DBConnection;

class BaseModel
{
    protected \Doctrine\DBAL\Connection $dbConnection;

    /**
     * Constructor for all models.
     *
     * @param DBConnection $dbConnection The database connection object.
     */
    public function __construct(DBConnection $dbConnection)
    {
        $this->dbConnection = $dbConnection->getConnection();
    }
}