<?php

namespace App\src\Models;

use Doctrine\DBAL\Exception;

class Author extends BaseModel
{
    /**
     * Fetches all authors from the database.
     *
     * @return array
     * @throws Exception
     */
    public function getAll(): array
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        return  $queryBuilder->select('*')
            ->from('authors')
            ->fetchAllAssociative();

    }

    /**
     * Inserts a new author into the database.
     * @param string $first_name The first name of the author.
     * @param string $last_name The last name of the author.
     * @param string $initials The initials of the author, generated from the first letter of each word in their name.
     * @param string $date_of_birth The date of birth of the author.
     * @param string $author_biography
     *
     * @return void
     * @throws Exception if the insertion fails.
     */
    public function insert(string $first_name, string $last_name,
                           string $date_of_birth, string $author_biography
    ): void
    {
        // Generate author initials
        $name_parts = explode(' ', $first_name);
        $last_name_parts = explode(' ', $last_name);
        $initials = strtoupper($name_parts[count($name_parts)-1][0] . $last_name_parts[count($last_name_parts)-1][0]);

        // Create an array of data to be inserted into the database.
        $insertData = [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'initials' => $initials,
            'date_of_birth' => $date_of_birth,
            'author_biography' => $author_biography
        ];

        // Insert the data into the 'authors' table using the database connection.
        // Throws an exception if the insertion fails.
        $this->dbConnection->insert('authors', $insertData);

        // Get the ID of the last inserted row.
        // This line doesn't do anything with the ID, so you may want to modify the method to return it or store it.
        $this->dbConnection->lastInsertId();
    }
}