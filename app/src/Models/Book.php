<?php

namespace App\src\Models;

use Doctrine\DBAL\Exception;

class Book extends BaseModel
{
    /**
     * Fetches all books from the database.
     *
     * @return array
     * @throws Exception
     */
    public function showAll(): array
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        return $queryBuilder->select('b.*', 'a.*', 'c.*')
            ->from('books', 'b')
            ->join('b', 'authors', 'a', 'b.book_author_id = a.author_id')
            ->join('b', 'book_categories', 'c', 'b.book_category_id = c.category_id')
            ->fetchAllAssociative();

    }

    /**
     * Fetches a book by ID from the database.
     *
     * @param int $id The book ID.
     *
     * @return array|bool
     * @throws Exception
     */
    public function getById(int $id): array|bool
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        return $queryBuilder
            ->select('*')
            ->from('books')
            ->where('book_id = :id')
            ->setParameter('id', $id)
            ->fetchAssociative();
    }

    /**
     * Creates a new book in the database.
     *
     * @param int $authorId
     * @param int $categoryId
     * @param string $isbn
     * @param string $publicationDate
     * @param string $title
     * @param float $price
     * @param string $description
     *
     * @return void
     * @throws Exception Throws an exception if the database operation fails.
     */
    public function insert(
        int    $authorId, int $categoryId, string $isbn, string $publicationDate,
        string $title, float $price, string $description
    ): void
    {
        // Create an array of data to be inserted into the database.
        $insertData = [
            'book_author_id' => $authorId,
            'book_category_id' => $categoryId,
            'isbn' => $isbn,
            'date_of_publication' => $publicationDate,
            'book_title' => $title,
            'price' => $price,
            'book_description' => $description
        ];


        // Insert the data into the 'books' table using the database connection.
        // Throws an exception if the insertion fails.
        $this->dbConnection->insert('books', $insertData);

        // Get the ID of the last inserted row.
        // This line doesn't do anything with the ID, so you may want to modify the method to return it or store it somewhere.
        $this->dbConnection->lastInsertId();
    }

    /**
     * Updates the information of a book in the database.
     *
     * @param float $price The new price of the book.
     * @param int $bookId The ID of the book to be updated.
     * @param string $isbn The new ISBN of the book.
     * @param string $date_of_publication The new date of publication of the book.
     * @param string $book_title The new title of the book.
     * @param string $book_description The new description of the book.
     *
     * @return void
     * @throws Exception If an error occurs during the database update.
     */
    public function update(
        float  $price, string $isbn, string $date_of_publication,
        string $book_title, string $book_description, int $bookId
    ): void
    {
        $updateData = [
            'isbn' => $isbn,
            'date_of_publication' => $date_of_publication,
            'book_title' => $book_title,
            'price' => $price,
            'book_description' => $book_description
        ];

        $this->dbConnection->update('books', $updateData, ['book_id' => $bookId]);
    }

    /**
     * Deletes a book from the database.
     *
     * @param int $bookId
     *
     * @return void
     * @throws Exception
     */
    public function delete(int $bookId): void
    {
        $this->dbConnection->delete('books', ['book_id' => $bookId]);
    }
}