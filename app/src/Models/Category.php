<?php

namespace App\src\Models;

use Doctrine\DBAL\Exception;

class Category extends BaseModel
{
    /**
     * Fetches all categories from the database.
     *
     * @return array
     * @throws Exception
     */
    public function getAll(): array
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        return  $queryBuilder->select('*')
            ->from('book_categories')
            ->fetchAllAssociative();

    }

    /**
     * Inserts a new book category into the database.
     *
     * @param string $category_name The name of the new category.
     * @param string $category_description The description of the new category.
     *
     * @return void
     * @throws Exception if the insertion fails.
     */
    public function insert(string $category_name, string $category_description): void
    {
        // Create an array of data to be inserted into the database.
        $insertData = [
            'category_name' => $category_name,
            'category_description' => $category_description
        ];

        // Insert the data into the 'book_categories' table using the database connection.
        // Throws an exception if the insertion fails.
        $this->dbConnection->insert('book_categories', $insertData);

        // Get the ID of the last inserted row.
        // This line doesn't do anything with the ID, so you may want to modify the method to return it or store it somewhere.
        $this->dbConnection->lastInsertId();
    }

}