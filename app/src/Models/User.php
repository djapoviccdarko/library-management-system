<?php

namespace App\src\Models;

use App\src\Database\DBConnection;
use Doctrine\DBAL\Exception;
use Psr\Log\InvalidArgumentException;

class User extends BaseModel
{
    /**
     * Fetches all users from the database.
     *
     * @return array Returns an array of user records.
     * @throws Exception Throws an exception if the database operation fails.
     */
    public function getAll(): array
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        return $queryBuilder
            ->select('*')
            ->from('users')
            ->fetchAllAssociative();
    }

    /**
     * Fetches a user by username from the database.
     *
     * @param string $username The username.
     *
     * @return array|bool Returns an array of user data if successful, false otherwise.
     * @throws Exception Throws an exception if the database operation fails.
     */
    public function getByUsername(string $username): array|bool
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        return $queryBuilder
            ->select('*')
            ->from('users')
            ->where('username = :username')
            ->setParameter('username', $username)
            ->fetchAssociative();
    }


    /**
     * Fetches a user by username and password from the database.
     *
     * @param string $username The username.
     * @param string $password
     * @return array|bool Returns an array of user data if successful, false otherwise.
     * @throws Exception Throws an exception if the database operation fails.
     */
    public function getByUsernameAndPassword(string $username, string $password): array|bool
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        $user = $queryBuilder
            ->select('*')
            ->from('users')
            ->where('username = :username')
            ->setParameter('username', $username)
            ->executeQuery()
            ->fetchAssociative();

        if (!$user) {
            return false;
        }

        // Use password_verify function to check if the user input matches the hashed password
        if (password_verify($password, $user['password'])) {
            return $user;
        } else {
            return false;
        }
    }

    /**
     * Fetches a user by email from the database.
     *
     * @param string $email The email.
     *
     * @return array|bool Returns an array of user data if successful, false otherwise.
     * @throws Exception Throws an exception if the database operation fails.
     */
    public function getByEmail(string $email): array|bool
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        return $queryBuilder
            ->select('*')
            ->from('users')
            ->where('email = :email')
            ->setParameter('email', $email)
            ->fetchAssociative();
    }

    /**
     * Inserts a new user into the database.
     *
     * @param string $name The user's name.
     * @param string $lastname The user's last name.
     * @param string $address The user's address.
     * @param string $email The user's email address.
     * @param string $username The user's username.
     * @param string $password The user's password.
     * @param string $phone The user's phone number.
     * @param string $city The user's city.
     *
     * @throws Exception Throws an exception if the database operation fails.
     */
    public function insert(
        string $name, string $lastname, string $address, string $email,
        string $username, string $password, string $phone, string $city,
    ): array
    {
        // Create an array of data to be inserted into the database.
        $insertData = [
            'name' => $name,
            'lastname' => $lastname,
            'address' => $address,
            'email' => $email,
            'username' => $username,
            'password' => $password,
            'phone' => $phone,
            'city' => $city,

        ];

        // Insert the data into the 'books' table using the database connection.
        // Throws an exception if the insertion fails.
        $this->dbConnection->insert('users', $insertData);

        // Get the ID of the last inserted row.
        // This line doesn't do anything with the ID, so you may want to modify the method to return it or store it somewhere.
        $this->dbConnection->lastInsertId();
        return $insertData;
    }

    /**
     * Updates a user in the database.
     *
     * @param int $userId The ID of the user to update.
     * @param string $name The new name of the user.
     * @param string $lastname The new last name of the user.
     * @param string $address The new address of the user.
     * @param string $email The new email of the user.
     * @param string $username The new username of the user.
     * @param string $password The new password of the user.
     * @param string $phone The new phone number of the user.
     * @param string $city The new city of the user.
     *
     * @return void
     * @throws Exception
     */
    public function update(
        int    $userId, string $name, string $lastname, string $address, string $email,
        string $username, string $password, string $phone, string $city
    ): void
    {
        $updateData = [
            'name' => $name,
            'lastname' => $lastname,
            'address' => $address,
            'email' => $email,
            'username' => $username,
            'password' => $password,
            'phone' => $phone,
            'city' => $city
        ];

        $this->dbConnection->update('users', $updateData, ['user_id' => $userId]);
    }

    /**
     * Deletes a user from the database.
     *
     * @param int $userId The ID of the user to delete.
     *
     * @return void
     *
     * @throws Exception
     */
    public function delete(int $userId): void
    {
        $this->dbConnection->delete('users', ['user_id' => $userId]);
    }

    /**
     * Update email verification status for a user.
     *
     * @param string $email The email address of the user.
     *
     * @return void
     * @throws Exception
     */
    public function updateEmailStatus(string $email): void
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        $queryBuilder->update('users')
            ->set('email_verified_at', 'CURRENT_TIMESTAMP()')
            ->where('email = :email')
            ->setParameter('email', $email);

        // Execute the update query
        $queryBuilder->executeStatement();

    }

    /**
     * Retrieves the email verification timestamp for the specified email address.
     *
     * @param string $email The email address for which to retrieve the verification timestamp.
     * @return string|null The timestamp of email verification, or null if the email is not found or has not been verified.
     * @throws Exception
     */
    public function getVerificationTime(string $email): ?string
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        $query = $queryBuilder
            ->select('email_verified_at')
            ->from('users')
            ->where('email = :email')
            ->setParameter('email', $email)
            ->executeQuery();

        // Fetch the first (and only) row from the query result and return the email verification timestamp.
        return $query->fetchOne();
    }

    /**
     * Check email verification by username.
     *
     * @param string $username The username for which to retrieve the verification timestamp.
     * @return string|null The timestamp of email verification, or null if the email is not found or has not been verified.
     * @throws Exception
     */
    public function checkVerificationByUsername(string $username): ?string
    {
        $queryBuilder = $this->dbConnection->createQueryBuilder();

        $query = $queryBuilder
            ->select('email_verified_at')
            ->from('users')
            ->where('username = :username')
            ->setParameter('username', $username)
            ->executeQuery();

        // Fetch the first (and only) row from the query result and return the email verification timestamp.
        return $query->fetchOne();
    }

}