<?php

namespace App\src\Database;

use App\Config;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Exception;

class DBConnection implements DBConnectionInterface
{
    private static ?self $instance = null;
    private Connection $connection;

    /**
     * DBConnection constructor.
     *
     * The constructor is private to prevent creating new instances from outside.
     * The getInstance() method should be used instead.
     * @throws Exception
     */
    private function __construct()
    {
        $config = new Configuration();
        $this->connection = DriverManager::getConnection(Config::CONNECTION_PARAMS, $config);
    }

    /**
     * Returns the singleton instance of the class.
     *
     * @return DBConnection
     */
    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Returns the Doctrine DBAL connection.
     *
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->connection;
    }
}
