<?php

namespace App\src\Controllers;

use App\Helpers;
use App\UserValidation;
use Exception;

class UserAuthController extends BaseController
{
    private UserValidation $validation;

    public function __construct()
    {
        parent::__construct();
        $this->validation = new UserValidation();
    }

    /**
     * Display login form.
     *
     * @return void
     * @throws Exception
     */
    public function loginPage(): void
    {
        echo $this->twig->render('users/user-login.twig', [
            'session' => $_SESSION
        ]);
    }

    /**
     * Display register form.
     *
     * @return void
     * @throws Exception
     */
    public function registerPage(): void
    {
        echo $this->twig->render('users/user-register.twig', [
            'session' => $_SESSION
        ]);
    }

    /**
     * This method inserts a new user into the database after validating the user input.
     *
     * @return void
     * @throws Exception
     */
    public function insert(): void
    {
        // Call to validate() method of the UserValidation object to validate the user input.
        $this->validation->validate();
    }

    /**
     * This method displays the user verification page and passes the data
     * received through the $_GET superglobal variable to the template.
     * @return void
     * @throws Exception
     */
    public function verifyPage(): void
    {
        // Retrieve the values passed in the $_GET super global variable and assign it to a variable called $data.
        $data = $_GET;

        // Use the Twig templating engine to render the user_verify.twig template and pass the $data variable to it.
        echo $this->twig->render('users/user_verify.twig', [
            'data' => $data
        ]);
    }

    /**
     * Verifies the email of a user and updates their email status in the database.
     *
     * @return void
     * @throws Exception
     */
    public function verifyEmail(): void
    {
        // Update email status in the database
        $this->userModel->updateEmailStatus($_POST['email']);

        // Get the verification time for the email from the database
        $verificationTime = $this->userModel->getVerificationTime($_POST['email']);

        // Render the user-login template with the verification time data
        echo $this->twig->render('users/user-login.twig', [
            'verificationTime' => $verificationTime
        ]);
    }

    /**
     * Checking the login parameters.
     * @throws Exception
     */
    public function login()
    {
        $this->validation->loginValidation();
    }

    /**
     * Destroying session.
     */
    public function logout()
    {
        session_unset();
        session_destroy();
        Helpers::redirect('/homePage');
    }
}
