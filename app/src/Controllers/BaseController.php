<?php

namespace App\src\Controllers;

use App\App;
use App\src\Database\DBConnection;
use App\src\Models\Author;
use App\src\Models\Book;
use App\src\Models\Category;
use App\src\Models\User;

class BaseController
{
    // Declare a private property to hold the database connection instance.
    private DBConnection $dbConnection;

    // Declare two protected properties to hold instances of the User and Book models.
    protected User $userModel;
    protected Book $bookModel;
    protected Author $authorModel;
    protected Category $categoryModel;

    // Declare a protected property to hold an instance of Twig.
    protected \Twig\Environment $twig;

    // Declare a constructor for the BaseController class.
    public function __construct()
    {
        // Get the singleton instance of the database connection.
        $this->dbConnection = DBConnection::getInstance();

        // Create new instances of the User and Book models, passing in the database connection instance.
        $this->userModel = new User($this->dbConnection);
        $this->bookModel = new Book($this->dbConnection);
        $this->authorModel = new Author($this->dbConnection);
        $this->categoryModel = new Category($this->dbConnection);

        // Load an instance of Twig using the App::twigLoader() method.
        $this->twig = App::twigLoader();

        // Add the DebugExtension to the Twig instance.
        $this->twig->addExtension(new \Twig\Extension\DebugExtension());
    }
}
