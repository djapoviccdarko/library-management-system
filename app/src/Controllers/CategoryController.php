<?php

namespace App\src\Controllers;

use App\Helpers;
use Doctrine\DBAL\Exception;

class CategoryController extends BaseController
{
    /**
     * Insert a new book category and redirect to the insert page
     *
     * @throws Exception
     */
    public function insert(): void
    {
        // Insert a new author into the database
        $this->categoryModel->insert(
            $_POST['category_name'],
            $_POST['category_description']
        );

        // Redirect to the index page
        Helpers::redirect("/insertPage");
    }
}