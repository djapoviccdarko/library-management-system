<?php

namespace App\src\Controllers;

class HomeController extends BaseController
{
    /**
     * Render the home page
     */
    public function homePage()
    {
        // Render the template with the data
        echo $this->twig->render('home-page.twig',[
            'session' => $_SESSION
        ]);
    }
}
