<?php

namespace App\src\Controllers;

use App\Helpers;
use Doctrine\DBAL\Exception;

class AuthorController extends BaseController
{
    /**
     * Insert a new author and redirect to the insert page
     * @throws Exception
     */
    public function insert(): void
    {
        // Insert a new author into the database
        $this->authorModel->insert(
            $_POST['first_name'],
            $_POST['last_name'],
            $_POST['date_of_birth'],
            $_POST['biography']
        );

        // Redirect to the index page
        Helpers::redirect("/insertPage");
    }
}