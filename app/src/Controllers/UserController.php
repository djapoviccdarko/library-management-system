<?php

namespace App\src\Controllers;

use App\Helpers;
use Doctrine\DBAL\Exception;

class UserController extends BaseController
{
    /**
     * Retrieves all users from the database and renders them using the index view.
     *
     * @throws \Exception
     */
    public function getAll(): void
    {
        $allUsers = $this->userModel->getAll();
        echo $this->twig->render('users/users-index.twig', [
            'users' => $allUsers,
            'session' => $_SESSION
        ]);
    }

    /**
     * Retrieves user from the database by username.
     *
     * @throws Exception
     */
    public function selectByUsername(string $username): array|bool
    {
        return $this->userModel->getByUsername($username);
    }

    /**
     * Retrieves user from the database by email.
     *
     * @throws Exception
     */
    public function selectByEmail(string $email): array|bool
    {
        return $this->userModel->getByEmail($email);
    }

    /**
     * Delete a user and redirect to the index page.
     *
     * @throws Exception
     */
    public function delete(): void
    {
        $this->userModel->delete(5);
        Helpers::redirect('/getAllUsers');
    }

    /**
     * Updates a user's information in the database and redirects to the index page.
     *
     * @throws Exception
     */
    public function update(): void
    {
        $this->userModel->update(
            1, 'petar', 'petrovic', 'svetog save', 'pero@gmail.com',
            'perop', 'perop00', '554806', 'ue'
        );
        Helpers::redirect('/getAllUsers');
    }
}
