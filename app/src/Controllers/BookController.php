<?php

namespace App\src\Controllers;

use App\Helpers;
use DateTime;
use Doctrine\DBAL\Exception;

class BookController extends BaseController
{
    /**
     * Show all books and render the index view
     * @throws \Exception
     */
    public function showAll(): void
    {
        // Get all the books from the database
        $allBooks = $this->bookModel->showAll();

        // Render the index view with the data
        echo $this->twig->render('books/books-index.twig', [
            'books' => $allBooks,
            'session' => $_SESSION
        ]);
    }

    /**
     * Render the index page with data
     * @throws \Exception
     */
    public function insertPage(): void
    {

        $authors = $this->authorModel->getAll();
        $categories = $this->categoryModel->getAll();

        // Render the user-login template with the verification time data
        echo $this->twig->render('books/insert-book.twig', [
            'authors' => $authors,
            'categories' => $categories,
            'session' => $_SESSION
        ]);
    }

    /**
     * Insert a new book and redirect to the index page
     * @throws Exception
     */
    public function insert(): void
    {
        // Insert a new book into the database
        $this->bookModel->insert(
            intval($_POST['author']),
            intval($_POST['category']),
            $_POST['isbn'],
            $_POST['date'],
            $_POST['title'],
            $_POST['price'],
            $_POST['description'],
        );

        // Redirect to the index page
        Helpers::redirect("/showAllBooks");
    }

    /**
     * Delete a book and redirect to the index page
     *
     * @throws Exception
     */
    public function delete(): void
    {
        // Delete a book from the database
        $this->bookModel->delete($_GET['book_id']);

        // Redirect to the index page
        Helpers::redirect("/showAllBooks");
    }

    /**
     * Render the update page with data
     *
     * @throws \Exception
     */
    public function updatePage(): void
    {
        $book = $this->bookModel->getById($_GET['book_id']);

        // Render the user-login template with the verification time data
        echo $this->twig->render('books/update-book.twig', [
            'book' => $book,
            'session' => $_SESSION
        ]);
    }

    /**
     * Update a book's price and redirect to the index page
     *
     * @throws Exception
     */
    public function update(): void
    {
        // Update a book's price in the database
        $this->bookModel->update(
            $_POST['price'],
            $_POST['isbn'],
            $_POST['date_of_publication'],
            $_POST['book_title'],
            $_POST['book_description'],
            $_POST['book_id']
        );

        // Redirect to the index page
        Helpers::redirect("/showAllBooks");
    }
}
