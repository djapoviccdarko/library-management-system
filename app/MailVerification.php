<?php

namespace App;

use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;

class MailVerification
{
    /**
     * Sends a verification email to the given email address.
     *
     * @param string $name The name of the recipient.
     * @param string $email The email address to send the verification email to.
     *
     * @return void
     * @throws TransportExceptionInterface
     */
    public static function sendVerificationEmail(string $name, string $email): void
    {
        $email = (new Email())
            ->from('redda999@gmail.com')
            ->to($email)
            ->subject('Verify email!')
            ->html("<p>Dear $name,</p>
                    <p>Please click the following link to verify your profile:</p>
                    <p><a href=\"http://localhost/verifyPage?email=" . urlencode($email) . "\">Click here to verify!</a></p>");

        // create a new SMTP Transport using the MailHog SMTP server
        $dsn = 'smtp://redda999@gmail.com:rlhvnbrfcnhqjhrr@smtp.gmail.com';
        $transport = Transport::fromDsn($dsn);

        // create a new Mailer instance using the SMTP Transport
        $mailer = new Mailer($transport);

        // send the email message using the Mailer instance
        $mailer->send($email);
    }
}